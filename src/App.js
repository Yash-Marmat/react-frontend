import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import NavBar from './components/NavBar'
import RegisterPage from './components/RegisterPage'
import LoginPage from './components/LoginPage'
import AccountPage from './components/AccountPage'
import AccountUpdatePage from './components/AccountUpdatePage'


function App() {
    return (
        <Router>
            <NavBar />
            <div className="container mt-4">
                <Route path="/" component={AccountPage} exact />
                <Route path="/update" component={AccountUpdatePage} exact />
                <Route path="/login" component={LoginPage} exact />
                <Route path="/register" component={RegisterPage} exact />
            </div>
        </Router>
    )
}

export default App